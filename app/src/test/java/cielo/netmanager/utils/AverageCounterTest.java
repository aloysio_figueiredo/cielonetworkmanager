package cielo.netmanager.utils;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by Aloysio Figueiredo (aloysio.figueiredo@m4u.com.br) on 18/05/16.
 */
public class AverageCounterTest {
    private static final double DELTA = 1e-15;

    private static AverageCounterStorage dummy = new AverageCounterStorage() {
        @Override
        public void store(AverageCounter elem) {

        }

        @Override
        public void retrieve(AverageCounter elem) {

        }
    };

    @Test
    public void test_setup() throws Exception {
        AverageCounter avg = new AverageCounter("dummy", 5, dummy);

        assertEquals(avg.getAverage(), 0, DELTA);
        assertEquals(avg.getCounter(), 0);
    }

    @Test
    public void test_addAverage() throws Exception {
        AverageCounter avg = new AverageCounter("dummy", 2, dummy);
        avg.addAverage(2);

        assertEquals(avg.addAverage(4), 3, DELTA);
    }


    @Test
    public void test_fullCycle() throws Exception {
        AverageCounter avg = new AverageCounter("dummy", 2, dummy);

        avg.addAverage(2);
        avg.addAverage(4);

        assertEquals(avg.addAverage(0), 0, DELTA);
    }

    @Test
    public void test_storeAndLoad() throws Exception {
        AverageCounterStorage mapstorage = new AverageCounterStorage() {

            private Map<String, AverageCounter> map = new HashMap<String, AverageCounter>();

            @Override
            public void store(AverageCounter elem) {
                map.put(elem.getId(), elem);
            }

            @Override
            public void retrieve(AverageCounter elem) {
                if (map.containsKey(elem.getId())) {
                    AverageCounter ac = map.get(elem.getId());
                    elem.copy(ac);
                }
            }
        };

        AverageCounter ac1 = new AverageCounter("ID1234", 2, mapstorage);

        ac1.addAverage(2);

        AverageCounter ac2 = new AverageCounter("ID1234", 2, mapstorage);

        assertEquals(ac2.getAverage(), 2, DELTA);
        assertEquals(ac2.getCounter(), 1);

    }
}
