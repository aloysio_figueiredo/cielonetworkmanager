package cielo.netmanager.utils;

import java.util.Collection;

/**
 * Created by Aloysio Figueiredo (aloysio.figueiredo@m4u.com.br) on 18/05/16.
 */
public class CompositeQualityCriterion implements QualityCriterion {

    private Collection<QualityCriterion> criteria = null;

    public CompositeQualityCriterion(Collection<QualityCriterion> criteria) {
        this.criteria = criteria;
    }


    @Override
    public boolean evaluate(long time) {
        boolean result = true;

        for (QualityCriterion c : criteria) {
            result = result && c.evaluate(time);
        }

        return result;
    }

}
