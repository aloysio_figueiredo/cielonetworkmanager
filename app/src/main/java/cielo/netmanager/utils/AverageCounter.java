package cielo.netmanager.utils;

/**
 * Created by Aloysio Figueiredo (aloysio.figueiredo@m4u.com.br) on 18/05/16.
 */
public class AverageCounter {

    private long timestamp;
    private int countLimit;
    private int counter;
    private float average;
    private AverageCounterStorage storage;
    private String id;

    private static AverageCounterStorage sharedPreferencesStorageInstance =
            new SharedPreferencesStorage();


    public AverageCounter(String id, int countLimit, AverageCounterStorage storage) {
        this.countLimit = countLimit;
        this.storage = storage;
        this.timestamp = 0;
        this.id = id;

        // retrieve current counter, timestamp and average
        storage.retrieve(this);
    }

    public AverageCounter(String id, int countLimit) {
        this(id, countLimit, sharedPreferencesStorageInstance);
    }

    public void copy(AverageCounter other) {
        counter = other.counter;
        average = other.average;
        timestamp = other.timestamp;
    }

    public String getId() {
        return id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public int getCounter() {
        return this.counter;
    }

    public void reset() {
        counter = 0;
        average = 0f;
        storage.store(this);
    }

    public float getAverage() {
        return this.average;
    }

    public float addAverage(float avg) {
        if (counter >= countLimit) {
            counter = 0;
            average = 0f;
        }

        average = (counter * average  + avg) / (counter + 1);
        counter += 1;

        storage.store(this);

        return average;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public void setAverage(float average) {
        this.average = average;
    }

    public void setCountLimit(int counter) {
        this.counter = counter;
    }
}
