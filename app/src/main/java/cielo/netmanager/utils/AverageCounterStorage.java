package cielo.netmanager.utils;

/**
 * Created by Aloysio Figueiredo (aloysio.figueiredo@m4u.com.br) on 18/05/16.
 */
public interface AverageCounterStorage {
    void store(AverageCounter elem);
    void retrieve(AverageCounter elem);
}
