package cielo.netmanager.utils;

import android.content.Context;
import android.content.SharedPreferences;

import cielo.netmanager.service.CieloNetworkManagerApp;

/**
 * Created by Aloysio Figueiredo (aloysio.figueiredo@m4u.com.br) on 18/05/16.
 */

public class SharedPreferencesStorage implements AverageCounterStorage {

    private final String spname = "AverageCounterStorage";

    @Override
    public void store(AverageCounter ac) {
        long timestamp = System.currentTimeMillis();
        Context ctx = CieloNetworkManagerApp.getAppContext();
        SharedPreferences sp = ctx.getSharedPreferences(spname, ctx.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        String value = String.format("%ld_%f_%d", timestamp, ac.getAverage(), ac.getCounter());

        editor.putString(ac.getId(), value);

        editor.commit();
    }

    @Override
    public void retrieve(AverageCounter ac) {
        Context ctx = CieloNetworkManagerApp.getAppContext();
        SharedPreferences sp = ctx.getSharedPreferences(spname, ctx.MODE_PRIVATE);

        String val = sp.getString(ac.getId(), "0_0_0");

        String[] parts = val.split("_");

        ac.setTimestamp(Long.valueOf(parts[0]));
        ac.setAverage(Float.valueOf(parts[1]));
        ac.setCountLimit(Integer.valueOf(parts[2]));

    }

}
