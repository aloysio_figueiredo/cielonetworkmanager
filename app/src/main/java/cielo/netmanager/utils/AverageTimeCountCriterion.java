package cielo.netmanager.utils;

/**
 * Created by Aloysio Figueiredo (aloysio.figueiredo@m4u.com.br) on 18/05/16.
 */
public class AverageTimeCountCriterion implements QualityCriterion {

    AverageCounter counter;
    long timeout;
    int timeLimit;
    int countLimit;

    public AverageTimeCountCriterion(long timeout, int timeLimit, int countLimit) {
        String id = String.format("%d_%d_%d", timeout, timeLimit, countLimit);
        this.timeout = timeout;
        this.timeLimit = timeLimit;
        this.countLimit = countLimit;
        /*
        We are implicitly using SharedPreferences to save the averages
        This can be easily changed by passing another AverageCounterStore to the constructor:
        counter = new AverageCounter(id, countLimit, anotherAverageCounterStore);
	    */
        counter = new AverageCounter(id, countLimit);
    }

    @Override
    public boolean evaluate(long time) {

        if (time < 0) return false;

        if (time - counter.getTimestamp() >= timeout) {
            counter.reset();
        }

        int count = counter.getCounter();
        float average = counter.addAverage(time);

        return (average < timeLimit && count < countLimit);

    }
}
