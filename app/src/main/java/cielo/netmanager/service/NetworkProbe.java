package cielo.netmanager.service;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import cielo.netmanager.utils.AverageTimeCountCriterion;
import cielo.netmanager.utils.QualityCriterion;

import static cielo.netmanager.service.Consts.*;

/**
 * Created by Aloysio Figueiredo on 26/04/16.
 */
public class NetworkProbe implements Runnable {

    private final String TAG = this.getClass().getName();

    private final CieloNetworkManager manager;

    public NetworkProbe(CieloNetworkManager manager) {
        this.manager = manager;
    }

    private long measureDownloadTime() {
        long start = System.currentTimeMillis();
        long duration = 0;
        try {
            Log.i(TAG, String.format("Starting network probe through URL %s  with %sms timeout",
                    REFERENCE_URL, CONNECTION_TIMEOUT));
            //HttpURLConnection.setFollowRedirects(false);
            HttpURLConnection con = (HttpURLConnection) new URL(REFERENCE_URL).openConnection();
            con.setRequestMethod("HEAD");

            con.setConnectTimeout(CONNECTION_TIMEOUT);


            con.connect();
            InputStream is = con.getInputStream();

            byte[] buffer = new byte[1024];
            while (is.read(buffer) > 0) {
                // do nothing
            }
            duration = System.currentTimeMillis() - start;


            //Log.i(TAG, String.format("response code: %d", con.getResponseCode()));



        } catch (java.net.SocketTimeoutException e) {
            Log.e(TAG, String.format("got %s after %d ms", e.toString(), CONNECTION_TIMEOUT));
            return System.currentTimeMillis() - start;
        } catch (java.io.IOException e) {
            Log.e(TAG, "got " + e.toString());
            return -1;
        }

        return duration;
    }

    private NetworkInfo getNetworkInfo() {
        ConnectivityManager cm = (ConnectivityManager) manager.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    private boolean isWifiConnection(){
        NetworkInfo info = getNetworkInfo();
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI);
    }

    private void disableWiFi() {
        Log.i(TAG, "disabling WiFi");
        WifiManager wifi = (WifiManager) manager.getSystemService(Context.WIFI_SERVICE);
        wifi.setWifiEnabled(false);
    }

    @Override
    public void run() {


        if (isWifiConnection()) {
            // We expect that the average of 5 connection attempts to be at most 4 minutes;
            // We disregard old connection attempts (too old : more than 2 minutes)
            QualityCriterion qc = new AverageTimeCountCriterion(2*60*1000, 4*60*1000, 5);
            long downloadTime = measureDownloadTime();

            if (qc.evaluate(downloadTime) == false) {
                disableWiFi();
            } else {
                Log.i (TAG, "WiFi connection is OK");
            }
        } else {
            Log.i(TAG, "Not running on WiFi");
        }
    }
}
