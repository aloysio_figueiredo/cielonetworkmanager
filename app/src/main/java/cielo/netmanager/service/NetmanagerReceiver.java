package cielo.netmanager.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NetmanagerReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        /* Nothing special here, just create an explicit intent to dispatch the action to
         * CieloNetworkManager
         *
         * In the future, this receiver may work as a facade to different services
         * or components
         */
        Intent NetmanagerIntent = new Intent(context, CieloNetworkManager.class);
        NetmanagerIntent.setAction(intent.getAction());
        NetmanagerIntent.putExtras(intent);

        context.startService(NetmanagerIntent);
    }
}
