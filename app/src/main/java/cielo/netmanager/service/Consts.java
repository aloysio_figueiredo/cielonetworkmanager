package cielo.netmanager.service;

/**
 * Created by aloysio on 17/05/16.
 */
public final class Consts {
    private Consts() {}
    public static final String START_SERVER_ACTION = "cielo.netmanager.START_SERVICE";
    public static final String STOP_SERVER_ACTION = "cielo.netmanager.STOP_SERVICE";
    public static final String NETPROBE_ACTION = "cielo.netmanager.NETPROBE";

    public static final int CONNECTION_TIMEOUT = 5000;
    public static final int READ_TIMEOUT = 5000;
    public static final String REFERENCE_URL = "http://www.google.com/";

    public static final int PROBE_FREQUENCY = 60;

}
