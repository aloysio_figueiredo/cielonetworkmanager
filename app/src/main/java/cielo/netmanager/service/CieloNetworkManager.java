package cielo.netmanager.service;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

public class CieloNetworkManager extends IntentService {

    private final String TAG="CieloNetworkManager";

    private final NetworkProbe networkProbe;

    public CieloNetworkManager() {
        super(CieloNetworkManager.class.getName());
        networkProbe = new NetworkProbe(this);
    }

    public void probeAndSchedule(int seconds) {

        HandlerThread ht = new HandlerThread("probeAndSchedule");
        ht.start();

        Handler handler = new Handler(ht.getLooper());

        Log.i(TAG, "Starting background thread to probe the network...");
        handler.post(networkProbe);

        Intent i = new Intent(this, this.getClass());
        i.setAction(Consts.NETPROBE_ACTION);

        PendingIntent pi = PendingIntent.getService(this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager amgr =  (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        long ct = System.currentTimeMillis();
        amgr.set(AlarmManager.RTC, ct + seconds * 1000, pi);

        stopSelf();
    }

    private void fullStop() {
        Intent i = new Intent(this, this.getClass());
        PendingIntent pi = PendingIntent.getService(this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager amgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);


        pi.cancel();
        amgr.cancel(pi);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i(TAG, "Handling new intent");
        String act = intent.getAction();

        switch (act) {
            case Consts.START_SERVER_ACTION:
            case Consts.NETPROBE_ACTION:
                probeAndSchedule(Consts.PROBE_FREQUENCY);
                break;
            case Consts.STOP_SERVER_ACTION:
                fullStop();
                break;
            default:
                String cls = this.getClass().getName();
                String msg = String.format("Invalid action <%s> in %s", act, cls);

                throw new IllegalArgumentException (msg);
        }

    }

}
