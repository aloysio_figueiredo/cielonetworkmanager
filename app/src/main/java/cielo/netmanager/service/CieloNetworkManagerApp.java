package cielo.netmanager.service;

import android.app.Application;
import android.content.Context;

/**
 * Created by Aloysio Figueiredo (aloysio.figueiredo@m4u.com.br) on 18/05/16.
 */
public class CieloNetworkManagerApp extends Application {

    private static Context context;

    public void onCreate() {
        super.onCreate();
        CieloNetworkManagerApp.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return CieloNetworkManagerApp.context;
    }

}
