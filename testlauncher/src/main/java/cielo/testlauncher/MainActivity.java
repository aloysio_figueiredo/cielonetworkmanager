package cielo.testlauncher;

import android.content.ComponentName;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addListenerOnButton();
    }

    private void addListenerOnButton() {
        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent i = new Intent("cielo.netmanager.service.CieloNetworkManager.DO_FOO");
                Intent i = new Intent();
                i.setComponent(new ComponentName("cielo.netmanager","cielo.netmanager.service.CieloNetworkManager"));
                //i.setAction("service.CieloNetworkManager.DO_FOO");
                //i.addCategory("android.intent.category.LAUNCHER");
                startService(i);
            }
        });
    }
}
